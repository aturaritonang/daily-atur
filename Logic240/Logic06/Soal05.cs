﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    class Soal05
    {
        private string[,] arr2d;

        public Soal05()
        {
            Console.WriteLine("--- Connected Cells in a Grid ---");
            Console.Write("Masukkan array 2 dimensi : ");
            string[] splitRow = Console.ReadLine().Split(',');
            string[] splitCol = splitRow[0].Split(' ');
            arr2d = new string[splitRow.Length, splitCol.Length];

            Console.WriteLine("Dimensi arr2d {0},{1}", arr2d.GetLength(0), arr2d.GetLength(1));

            for (int row = 0; row < splitRow.Length; row++)
            {
                splitCol = splitRow[row].Split(' ');
                for (int col = 0; col < splitCol.Length; col++)
                {
                    arr2d[row, col] = splitCol[col];
                    Console.Write(int.Parse(splitCol[col]) + "\t");
                    //pindah kolom
                }
                Console.WriteLine();
                //pindah baris
            }

            int lastReg = 1;

            for (int row = 0; row < arr2d.GetLength(0); row++)
            {
                for (int col = 0; col < arr2d.GetLength(1); col++)
                {
                    if (arr2d[row, col] == "1")
                        ReplaceRegion(row, col, (++lastReg).ToString());
                    else if (arr2d[row, col] != "0")
                        ReplaceRegion(row, col, arr2d[row, col]);
                }
                //Console.WriteLine("\n");
            }

            Console.WriteLine("Illustrasi\n");

            int[] jml = new int[lastReg];

            for (int i = 0; i < arr2d.GetLength(0); i++)
            {
                for (int j = 0; j < arr2d.GetLength(1); j++)
                {
                    if (int.Parse(arr2d[i, j]) > 0)
                        jml[int.Parse(arr2d[i, j]) - 1] += 1;
                    Console.Write(arr2d[i, j] + "\t");
                }
                Console.WriteLine("\n");
            }
            Console.WriteLine("Max : {0}", jml.Max());
            Console.ReadKey();
        }

        private void ReplaceRegion(int row, int col, string reg)
        {
            for (int i = row - 1; i <= row + 1; i++)
            {
                for (int j = col - 1; j <= col + 1; j++)
                {
                    if (i >= 0 && i <= arr2d.GetLength(0) - 1 && j >= 0 && j <= arr2d.GetLength(1) - 1)
                    {
                        if (int.Parse(arr2d[i, j]) < int.Parse(reg) && int.Parse(arr2d[i, j]) > 1)
                        {
                            ReplaceAllRegion(reg, arr2d[i, j]);
                            reg = arr2d[i, j];
                        }
                        
                        if (arr2d[i, j] != "0")
                            arr2d[i, j] = reg;
                    }
                }
            }
        }

        private void ReplaceAllRegion(string oldVal, string newVal)
        {
            for (int i = 0; i < arr2d.GetLength(0); i++)
            {
                for (int j = 0; j < arr2d.GetLength(1); j++)
                {
                    if (arr2d[i, j] == oldVal)
                        arr2d[i, j] = newVal;
                }
            }
        }
    }
}
