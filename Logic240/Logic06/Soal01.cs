﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    class Soal01
    {
        public Soal01()
        {
            Console.WriteLine("-- Sherlock and Array ---");
            Console.Write("Masukkan Deret Angka dgn spasi : ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), val => Convert.ToInt32(val));

            // Cari posisi utama
            for (int i = 0; i < arr.Length; i++)
            {
                // Hitung sebelah kiri
                int iKiri = 0;
                if (i > 0)
                {
                    for (int j = 0; j < i; j++)
                    {
                        iKiri += arr[j];
                    }
                }
                // Hitung sebelah kanan
                int iKanan = 0;
                if (i < arr.Length - 1)
                {
                    for (int j = i + 1; j < arr.Length; j++)
                    {
                        iKanan += arr[j];
                    }
                }

                if (iKiri == iKanan)
                {
                    Console.WriteLine("YES, hasil kiri: {0}, kanan {1}", iKiri, iKanan);
                    break;
                }
                else
                {
                    Console.WriteLine("NO, hasil kiri: {0}, kanan {1}", iKiri, iKanan);
                }
            }

            Console.ReadKey();
        }
    }
}
