﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic01
{
    class Soal01
    {
        public Soal01()
        {
            Console.Write("Masukkan nilai N: ");
            int n = int.Parse(Console.ReadLine());
            Cetak(n);
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue!");
            Console.ReadKey();
        }

        private void CetakTambah(int Nil1, int Nil2)
        {
            Console.WriteLine("Hasilnya dari {0} + {1} = {2}", Nil1, Nil2, Nil1 + Nil2);
        }

        private int NilaiTambah(int Nil1, int Nil2)
        {
            return Nil1 + Nil2;
        }

        private void Cetak(int n)
        {
            for (int I = 0; I < n; I++)
            {
                Console.Write(I * 2 + 1 + "\t");
            }
        }
    }
}
