﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic01
{
    class Soal02
    {
        public Soal02()
        {
            Console.Write("Masukkan nilai N: ");
            int n = int.Parse(Console.ReadLine());
            Cetak(n);
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue!");
            Console.ReadKey();
        }

        private void Cetak(int n)
        {
            for (int I = 0; I < n; I++)
            {
                Console.Write(I * 2 + 2 + "\t");
            }
        }
    }
}
