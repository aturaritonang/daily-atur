﻿using Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic02
{
    class Soal03 : LogicProps
    {
        public Soal03(int n)
        {
            Array2D = new string[2, n];
            FillArray();
            LogicFunction.PrintArray(Array2D);
        }

        private void FillArray()
        {
            for (int i = 0; i < Array2D.GetLength(1); i++)
            {
                Array2D[0, i] = i.ToString();
                if (i <= Array2D.GetLength(1) / 2)
                {
                    Array2D[1, i] = (3 * (i + 1)).ToString();
                    Array2D[1, Array2D.GetLength(1) - i - 1] = (3 * (i + 1)).ToString();
                }
            }
        }
    }
}
