﻿using Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic02
{
    class Soal01: LogicProps
    {
        public Soal01(int n)
        {
            Array2D = new string[2, n];
            FillArray();
            LogicFunction.PrintArray(Array2D);
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        private void FillArray()
        {
            //baris 0
            for (int I = 0; I < Array2D.GetLength(1); I++)
            {
                Array2D[0, I] = I.ToString();
            }

            //baris 1
            for (int I = 0; I < Array2D.GetLength(1); I++)
            {
                Array2D[1, I] = (Math.Pow(3, I)).ToString();
            }
        }
    }
}
