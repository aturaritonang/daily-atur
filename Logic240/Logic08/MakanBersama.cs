﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic08
{
    class MakanBersama
    {
        public MakanBersama()
        {
            Console.WriteLine("--- Makan Bersama ---");
            Console.Write("Masukkan list pesanan : ");
            double[] pesanan = Array.ConvertAll(Console.ReadLine().Split(','), hrg => double.Parse(hrg));

            double ber3 = pesanan[0] + (pesanan[0] * 0.15);
            double ber4 = 0;
            for (int i = 1; i < pesanan.Length; i++)
            {
                ber4 += pesanan[i];
            }
            ber4 = ber4 + (ber4 * 0.15);

            Console.WriteLine("Bertiga : {0}", ber3);
            Console.WriteLine("Berempat: {0}", ber4);
            Console.WriteLine("Yg alergi byr {0}", ber4 / 4);
            Console.WriteLine("Yg tdk alergi byr {0}", (ber4 / 4) + (ber3 / 3));
            Console.ReadKey();
        }
    }
}
