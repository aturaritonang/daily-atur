﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic03
{
    public class Soal03
    {
        public Soal03()
        {
            Console.Write("Masukkan score Alice dgn seperator ,:");
            string[] aliArr = Console.ReadLine().Split(',');
            Console.ReadLine();
            Console.Write("Masukkan score Bob dgn seperator ,:");
            string[] bobArr = Console.ReadLine().Split(',');

            int[] asd = CountScore(aliArr, bobArr);
            ShowResult(asd[0], asd[1]);
        }

        private int[] CountScore(string[] arr1, string[] arr2)
        {
            int[] result = new int[2] { 5, 7};
            int aliSc = 0;
            int bobSc = 0;

            for (int i = 0; i < arr1.Length; i++)
            {
                Console.WriteLine(arr1[i] + ":" + arr2[i]);
                if (int.Parse(arr1[i]) > int.Parse(arr2[i]))
                {
                    aliSc++;
                }
                else if (int.Parse(arr1[i]) < int.Parse(arr2[i]))
                {
                    bobSc++;
                }
            }

            result[0] = aliSc;
            result[1] = bobSc;
            return result;
        }

        private void ShowResult(int aliSc, int bobSc)
        {
            Console.WriteLine("Alice: {0}, Bob: {1}", aliSc, bobSc);
            Console.WriteLine("Alice: " + aliSc +  ", Bob: " + bobSc);
        }
    }
}
