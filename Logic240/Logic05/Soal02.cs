﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    class Soal02
    {
        public Soal02()
        {
            Console.WriteLine("-- Find the Median ---");
            Console.Write("Masukkan Deret Angka dgn spasi : ");
            decimal[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), val => Convert.ToDecimal(val));

            Array.Sort(arr);
            int midPos = 0;
            decimal median = 0;
            // Bila ganjil
            if (arr.Length % 2 == 1)
            {
                midPos = arr.Length / 2;
                Console.WriteLine("Nilai tengah : {0}", arr[midPos]);
                median = arr[midPos];
            }
            // Bila genap
            else
            {
                midPos = (arr.Length / 2 - 1);
                Console.WriteLine("Nilai tengah : {0} & {1}", arr[midPos], arr[midPos + 1]);
                median = (arr[midPos] + arr[midPos + 1]) / 2;
            }

            Console.WriteLine("Media : {0}", median);
            Console.ReadKey();
        }
    }
}
