﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    class Soal03
    {
        public Soal03()
        {
            Console.WriteLine("-- Closest Number ---");
            Console.Write("Masukkan Deret Angka dgn spasi : ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), val => Convert.ToInt32(val));

            Array.Sort(arr);

            int diff = 0;
            // Titik awal
            for (int i = 0; i < arr.Length - 1; i++)
            {
                //Pemberi nilai awal
                if (i == 0)
                    diff = Math.Abs(arr[i] - arr[i + 1]);

                // Titik akhir
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (Math.Abs(arr[i] - arr[i + 1]) < diff)
                    {
                        diff = Math.Abs(arr[i] - arr[i + 1]);
                    }
                }
            }

            Console.WriteLine("\nResult:");

            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (Math.Abs(arr[i] - arr[i + 1]) == diff)
                {
                    Console.Write("{0}\t{1}\t", arr[i], arr[i + 1]);
                }
            }
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
